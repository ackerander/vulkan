#version 450
layout(binding = 0) uniform UniformBuffObj {
    mat4 matrix;
} mvp;

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;

void main() {
    gl_Position = mvp.matrix * vec4(inPos, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}
