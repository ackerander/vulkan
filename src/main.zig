const std = @import("std");
const vulk = @import("vulkan.zig");

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const alloc = if (vulk.DEBUG_ENABLE) gpa.allocator() else std.heap.c_allocator;

pub fn main() !void {
    try vulk.init(alloc);
    try vulk.mainLoop();
    vulk.deinit();
}
