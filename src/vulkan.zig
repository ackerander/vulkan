const builtin = @import("builtin");
const std = @import("std");
const math = @import("math.zig");
const c = @cImport({
    @cDefine("GLFW_INCLUDE_VULKAN", {});
    @cInclude("GLFW/glfw3.h");
    @cInclude("stb/stb_image.h");
    @cDefine("FAST_OBJ_IMPLEMENTATION", {});
    @cInclude("fast_obj.h");
    @cInclude("string.h");
});

pub const DEBUG_ENABLE = builtin.mode == .Debug or builtin.mode == .ReleaseSafe;
const MAX_IN_FLIGHT = 2;
const required_extens = [_][]const u8{c.VK_KHR_SWAPCHAIN_EXTENSION_NAME};
inline fn vertSize() usize { return vk_data.vertices.len * @sizeOf(Vertex); }
inline fn idxSize() usize { return vk_data.indices.len * @sizeOf(u32); }
const UNI_SIZE = @sizeOf(math.Mat4);

var alloc: std.mem.Allocator = undefined;
const VkError = error {
    GLFWinitErr,
    VkNoSupport,
    WindowErr,
    VkInitErr,
    LayerNotAvail,
    DebugMessenger,
    NoGoodGPU,
    LogicalDevErr,
    SurfaceErr,
    SwapchainErr,
    ViewsErr,
    ShaderErr,
    UniformErr,
    PipelineErr,
    RenderPassErr,
    FrameBufferErr,
    CommandErr,
    DescriptorErr,
    BuffErr,
    TexErr,
    ImgErr,
    SyncErr,
    NoFormat,
    ModelErr,
};

var vk_data: struct {
    window: *c.GLFWwindow,
    surface: c.VkSurfaceKHR,

    instance: c.VkInstance,
    phys_dev: c.VkPhysicalDevice,
    dev: c.VkDevice,
    msaa: c.VkSampleCountFlagBits,

    graphics_queue: c.VkQueue,
    present_queue: c.VkQueue,

    swapchain: c.VkSwapchainKHR,
    swapchain_imgs: []c.VkImage,
    swapchain_fmt: c.VkFormat,
    swapchain_extent: c.VkExtent2D,
    swapchain_views: []c.VkImageView,
    swapchain_framebuffs: []c.VkFramebuffer,

    render_pass: c.VkRenderPass,
    descriptorset_layout: c.VkDescriptorSetLayout,
    descriptor_pool: c.VkDescriptorPool,
    pipeline_layout: c.VkPipelineLayout,
    pipeline: c.VkPipeline,

    cmd_pool: c.VkCommandPool,
    vert_buff: c.VkBuffer,
    vert_mem: c.VkDeviceMemory,
    idx_buff: c.VkBuffer,
    idx_mem: c.VkDeviceMemory,

    mip_levels: u32,
    tex_img: c.VkImage,
    tex_mem: c.VkDeviceMemory,
    tex_view: c.VkImageView,
    tex_sampler: c.VkSampler,
    depth_img: c.VkImage,
    depth_mem: c.VkDeviceMemory,
    depth_view: c.VkImageView,
    color_img: c.VkImage,
    color_mem: c.VkDeviceMemory,
    color_view: c.VkImageView,

    uniform_buffs: [MAX_IN_FLIGHT]c.VkBuffer,
    uniform_mem: [MAX_IN_FLIGHT]c.VkDeviceMemory,
    uniform_mapped: [MAX_IN_FLIGHT]?*anyopaque,
    descrptor_sets: [MAX_IN_FLIGHT]c.VkDescriptorSet,
    cmd_buffs: [MAX_IN_FLIGHT]c.VkCommandBuffer,
    img_semaphores: [MAX_IN_FLIGHT]c.VkSemaphore,
    render_semaphores: [MAX_IN_FLIGHT]c.VkSemaphore,
    in_flight_fences: [MAX_IN_FLIGHT]c.VkFence,

    vertices: []Vertex,
    indices: []u32,

    timer: std.time.Timer,
    current_frame: u32,
} = undefined;

// Utility functions
fn cstreql(a: []const u8, b: []const u8) bool {
    return for (0..b.len) |i| {
        if (a[i] != b[i]) break false;
    } else a[b.len] == 0;
}

///// Begin Setup /////
pub fn init(allocator: std.mem.Allocator) !void {
    alloc = allocator;
    // GLFW setup
    if (c.glfwInit() == 0) return VkError.GLFWinitErr;
    errdefer c.glfwTerminate();

    if (c.glfwVulkanSupported() == c.GLFW_FALSE) return VkError.VkNoSupport;

    c.glfwWindowHint(c.GLFW_CLIENT_API, c.GLFW_NO_API);
    c.glfwWindowHint(c.GLFW_RESIZABLE, c.GLFW_FALSE);
    // c.glfwWindowHint(c.GLFW_SAMPLES, 4); // 4x antialiasing
    const monitor = c.glfwGetPrimaryMonitor();
    const mon_mode = c.glfwGetVideoMode(monitor);
    c.glfwWindowHint(c.GLFW_RED_BITS, mon_mode.*.redBits);
    c.glfwWindowHint(c.GLFW_GREEN_BITS, mon_mode.*.greenBits);
    c.glfwWindowHint(c.GLFW_BLUE_BITS, mon_mode.*.blueBits);
    c.glfwWindowHint(c.GLFW_REFRESH_RATE, mon_mode.*.refreshRate);
    vk_data.window = c.glfwCreateWindow(mon_mode.*.width, mon_mode.*.height, "GLFW Zig", monitor, null)
        orelse return VkError.WindowErr;
    errdefer c.glfwDestroyWindow(vk_data.window);
    // c.glfwMakeContextCurrent(vk_data.window);

    // c.glfwSetInputMode(vk_data.window, c.GLFW_CURSOR, c.GLFW_CURSOR_DISABLED);
    // c.glfwSetCursorPos(vk_data.window, @as(f32, @floatFromInt(mon_mode.*.width)) / 2,
    //     @as(f32, @floatFromInt(mon_mode.*.height)) / 2);

    // Init Vulkan
    try createInstance();
    if (c.glfwCreateWindowSurface(vk_data.instance, vk_data.window, null, &vk_data.surface) !=
        c.VK_SUCCESS)
        return VkError.SurfaceErr;
    const dev_details = try pickPhysDev();
    defer dev_details.swapchain.deinit();
    try createLogicalDev(dev_details.indices);
    try createSwapchain(dev_details);
    try createImgViews();
    try createRenderPass();
    try createDescriptorSetLayout();
    try createGraphicsPipeline();
    try createCmdPool(dev_details.indices);
    try createColorResources();
    try createDepthResources();
    try createFramebuffers();
    try createTexImg();
    vk_data.tex_view = try createImgView(vk_data.tex_img, c.VK_FORMAT_R8G8B8A8_SRGB,
        c.VK_IMAGE_ASPECT_COLOR_BIT, vk_data.mip_levels);
    try createTexSampler();
    try loadModel();
    try createVertBuff();
    try createIdxBuff();
    try createUniformBuffs();
    try createDescriptorPool();
    try createDescriptorSets();
    try createCmdBuffs();
    try createSyncObjs();
}

fn createInstance() !void {
    const app_info = c.VkApplicationInfo {
        .sType = c.VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "Vulkan Zig",
        .applicationVersion = c.VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "None",
        .engineVersion = c.VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = c.VK_API_VERSION_1_3,
    };

    const extensions = try getExtensions();
    if (DEBUG_ENABLE) {
        if (!try checkValidationLayers()) return VkError.LayerNotAvail;
        var extension_count: u32 = undefined;
        _ = c.vkEnumerateInstanceExtensionProperties(null, &extension_count, null);
        const avail_extensions = try alloc.alloc(c.VkExtensionProperties, extension_count);
        defer alloc.free(avail_extensions);
        _ = c.vkEnumerateInstanceExtensionProperties(null, &extension_count, @ptrCast(avail_extensions));
        std.debug.print("Extensions:\n", .{});
        for (0..extension_count) |i|
            std.debug.print("\t{s}\n", .{avail_extensions[i].extensionName});
    }
    const create_info = c.VkInstanceCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &app_info,
        .enabledExtensionCount = @truncate(extensions.len),
        .ppEnabledExtensionNames = @ptrCast(extensions),
        .enabledLayerCount = if (DEBUG_ENABLE) validation_layers.len else 0,
        .ppEnabledLayerNames = if (DEBUG_ENABLE) @ptrCast(&validation_layers) else undefined,
        .pNext = if (DEBUG_ENABLE) &debug_messenger_create_info else null,
    };
    if (c.VK_SUCCESS != c.vkCreateInstance(&create_info, null, &vk_data.instance))
        return VkError.VkInitErr;

    if (DEBUG_ENABLE) {
        alloc.free(extensions);
        try setupDebugMessenger();
    }
}

fn getExtensions() ![][*c]const u8 {
    const additional_extensions = [_][*c]const u8{c.VK_EXT_DEBUG_UTILS_EXTENSION_NAME};

    var glfw_extensions_len: u32 = undefined;
    const glfw_extensions = c.glfwGetRequiredInstanceExtensions(&glfw_extensions_len);

    if (!DEBUG_ENABLE) return glfw_extensions[0..glfw_extensions_len];
    const data_len = glfw_extensions_len + additional_extensions.len;
    const data = try alloc.alloc([*c]const u8, data_len);
    @memcpy(data[0..glfw_extensions_len], glfw_extensions[0..glfw_extensions_len]);
    @memcpy(data[glfw_extensions_len..], &additional_extensions);
    return data;
}

const SwapchainDetails = struct {
    capabilities: c.VkSurfaceCapabilitiesKHR,
    formats: []c.VkSurfaceFormatKHR,
    present_modes: []c.VkPresentModeKHR,
    fn isGood(self: @This()) bool { return self.formats.len != 0 and self.present_modes.len != 0; }
    fn deinit(self: @This()) void {
        alloc.free(self.formats);
        alloc.free(self.present_modes);
    }
};
const DevDetails = struct {
    indices: [2]u32,
    swapchain: SwapchainDetails,
};
fn pickPhysDev() !DevDetails  {
    var dev_count: u32 = undefined;
    _ = c.vkEnumeratePhysicalDevices(vk_data.instance, &dev_count, null);
    if (dev_count == 0) return VkError.NoGoodGPU;
    const devices = try alloc.alloc(c.VkPhysicalDevice, dev_count);
    defer alloc.free(devices);
    _ = c.vkEnumeratePhysicalDevices(vk_data.instance, &dev_count, @ptrCast(devices));

    var best_score: u32 = 0;
    var best_dev: *const c.VkPhysicalDevice = undefined;
    var best_details: DevDetails = undefined;
    for (devices) |dev| {
        var properties: c.VkPhysicalDeviceProperties = undefined;
        var features: c.VkPhysicalDeviceFeatures = undefined;
        c.vkGetPhysicalDeviceProperties(dev, &properties);
        c.vkGetPhysicalDeviceFeatures(dev, &features);
        if (features.geometryShader == 0 or features.samplerAnisotropy == c.VK_FALSE or
        !checkDevExtensionSupport: {
            var exten_count: u32 = undefined;
            _ = c.vkEnumerateDeviceExtensionProperties(dev, null, &exten_count, null);
            const extensions = try alloc.alloc(c.VkExtensionProperties, exten_count);
            defer alloc.free(extensions);
            _ = c.vkEnumerateDeviceExtensionProperties(dev, null, &exten_count, @ptrCast(extensions));
            for (required_extens) |required| {
                for (extensions) |extension| {
                    if (cstreql(&extension.extensionName, required)) break;
                } else break :checkDevExtensionSupport false;
            }
            break :checkDevExtensionSupport true;
        }) continue;

        const indices: [2]u32 = findQueueFamilies: {
            var queue_fam_len: u32 = undefined;
            c.vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_fam_len, null);
            const queue_families = try alloc.alloc(c.VkQueueFamilyProperties, queue_fam_len);
            defer alloc.free(queue_families);
            c.vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_fam_len, @ptrCast(queue_families));
            break :findQueueFamilies .{
                for (queue_families, 0..) |queue_fam, i| {
                    if (queue_fam.queueFlags & c.VK_QUEUE_GRAPHICS_BIT != 0)
                        break @intCast(i);
                } else continue,
                for (0..queue_fam_len) |i| {
                    const idx: u32 = @intCast(i);
                    var supported: c.VkBool32 = undefined;
                    _ = c.vkGetPhysicalDeviceSurfaceSupportKHR(dev, idx, vk_data.surface, &supported);
                    if (supported == c.VK_TRUE)
                        break idx;
                } else continue,
            };
        };

        const swapchain_details = querySwapchainSupport: {
            var details: SwapchainDetails = undefined;
            _ = c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(dev, vk_data.surface, &details.capabilities);

            var formats_len: u32 = undefined;
            _ = c.vkGetPhysicalDeviceSurfaceFormatsKHR(dev, vk_data.surface, &formats_len, null);
            if (formats_len != 0) {
                details.formats = try alloc.alloc(c.VkSurfaceFormatKHR, formats_len);
                _ = c.vkGetPhysicalDeviceSurfaceFormatsKHR(dev, vk_data.surface, &formats_len, @ptrCast(details.formats));
            }

            var present_modes_len: u32 = undefined;
            _ = c.vkGetPhysicalDeviceSurfacePresentModesKHR(dev, vk_data.surface, &present_modes_len, null);
            if (present_modes_len != 0) {
                details.present_modes = try alloc.alloc(c.VkPresentModeKHR, present_modes_len);
                _ = c.vkGetPhysicalDeviceSurfacePresentModesKHR(dev, vk_data.surface, &present_modes_len,
                    @ptrCast(details.present_modes));
            }
            break :querySwapchainSupport details;
        };
        if (!swapchain_details.isGood()) {
            swapchain_details.deinit();
            continue;
        }

        const score = @as(u32,
            if (properties.deviceType == c.VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) 1000 else 0
            ) + properties.limits.maxImageDimension2D;

        if (score > best_score) {
            best_dev = &dev;
            if (best_score > 0)
                best_details.swapchain.deinit();
            best_details = .{.indices = indices, .swapchain = swapchain_details};
            best_score = score;
        } else {
            swapchain_details.deinit();
        }
    }
    if (best_score == 0) return VkError.NoGoodGPU;
    vk_data.phys_dev = best_dev.*;
    vk_data.msaa = getMaxSampleCount();
    return best_details;
}

fn chooseSurfaceFormat(avail_formats: []c.VkSurfaceFormatKHR) c.VkSurfaceFormatKHR {
    // Consider ranking formats
    return for (avail_formats) |format| {
        if (format.format == c.VK_FORMAT_B8G8R8A8_SRGB and format.colorSpace == c.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            break format;
    } else avail_formats[0];
}
fn choosePresentMode(avail_modes: []c.VkPresentModeKHR) c.VkPresentModeKHR {
    return for (avail_modes) |mode| {
        if (mode == c.VK_PRESENT_MODE_MAILBOX_KHR) break mode;
    } else c.VK_PRESENT_MODE_FIFO_KHR;
}
fn chooseExtent(capabilities: c.VkSurfaceCapabilitiesKHR) c.VkExtent2D {
    if (capabilities.currentExtent.width != std.math.maxInt(u32))
        return capabilities.currentExtent;
    var extens: [2]i32 = undefined;
    c.glfwGetFramebufferSize(vk_data.window, &extens[0], &extens[1]);
    return .{
        .width = std.math.clamp(@as(u32, @intCast(extens[0])), capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
        .height = std.math.clamp(@as(u32, @intCast(extens[1])), capabilities.minImageExtent.height, capabilities.maxImageExtent.height),
    };
}
fn createSwapchain(dev_details: DevDetails) !void {
    const surface_format = chooseSurfaceFormat(dev_details.swapchain.formats);
    const present_mode = choosePresentMode(dev_details.swapchain.present_modes);
    const extent = chooseExtent(dev_details.swapchain.capabilities);

    var image_count = dev_details.swapchain.capabilities.minImageCount +
        @as(u32, if (dev_details.swapchain.capabilities.minImageCount > 0 and
            dev_details.swapchain.capabilities.maxImageCount <=
            dev_details.swapchain.capabilities.minImageCount)
            0 else 1);

    var create_info = c.VkSwapchainCreateInfoKHR{
        .sType = c.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = vk_data.surface,
        .minImageCount = image_count,
        .imageFormat = surface_format.format,
        .imageColorSpace = surface_format.colorSpace,
        .imageExtent = extent,
        .imageArrayLayers = 1,
        .imageUsage = c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = dev_details.swapchain.capabilities.currentTransform,
        .compositeAlpha = c.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .clipped = c.VK_TRUE,
        .oldSwapchain = @ptrCast(c.VK_NULL_HANDLE),
    };
    if (dev_details.indices[0] != dev_details.indices[1]) {
        create_info.imageSharingMode = c.VK_SHARING_MODE_CONCURRENT;
        create_info.queueFamilyIndexCount = 2;
        create_info.pQueueFamilyIndices = &dev_details.indices;
    } else {
        create_info.imageSharingMode = c.VK_SHARING_MODE_EXCLUSIVE;
    }
    if (c.VK_SUCCESS != c.vkCreateSwapchainKHR(vk_data.dev, &create_info, null, &vk_data.swapchain))
        return VkError.SwapchainErr;
    _ = c.vkGetSwapchainImagesKHR(vk_data.dev, vk_data.swapchain, @ptrCast(&image_count), null);
    vk_data.swapchain_imgs = try alloc.alloc(c.VkImage, image_count);
    _ = c.vkGetSwapchainImagesKHR(vk_data.dev, vk_data.swapchain, &image_count, @ptrCast(vk_data.swapchain_imgs));
    vk_data.swapchain_fmt = surface_format.format;
    vk_data.swapchain_extent = extent;
}

fn createImgView(img: c.VkImage, fmt: c.VkFormat, flags: c.VkImageAspectFlags, mip: u32) !c.VkImageView {
    const create_info = c.VkImageViewCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = img,
        .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
        .format = fmt,
        .subresourceRange = .{
            .aspectMask = flags,
            .baseMipLevel = 0,
            .levelCount = mip,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    var view: c.VkImageView = undefined;
    return if (c.vkCreateImageView(vk_data.dev, &create_info, null, &view) != c.VK_SUCCESS)
        VkError.ViewsErr else view;
}
fn createImgViews() !void {
    vk_data.swapchain_views = try alloc.alloc(c.VkImageView, vk_data.swapchain_imgs.len);
    for (vk_data.swapchain_imgs, vk_data.swapchain_views) |img, *view|
        view.* = try createImgView(img, vk_data.swapchain_fmt, c.VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

fn createLogicalDev(indices: [2]u32) !void {
    const unique: u32 = if (indices[0] == indices[1]) 1 else 2;
    var queue_create_infos: [2]c.VkDeviceQueueCreateInfo = undefined;
    const queue_priority: f32 = 1;
    for (0..unique) |i|
        queue_create_infos[i] = .{
            .sType = c.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = indices[i],
            .queueCount = 1,
            .pQueuePriorities = &queue_priority,
        };
    const dev_features = c.VkPhysicalDeviceFeatures { .samplerAnisotropy = c.VK_TRUE };
    const create_info = c.VkDeviceCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pQueueCreateInfos = &queue_create_infos,
        .queueCreateInfoCount = unique,
        .pEnabledFeatures = &dev_features,
        .enabledLayerCount = 0,
        .enabledExtensionCount = required_extens.len,
        .ppEnabledExtensionNames = @ptrCast(&required_extens),
    };
    if (c.VK_SUCCESS != c.vkCreateDevice(vk_data.phys_dev, &create_info, null, &vk_data.dev))
        return VkError.LogicalDevErr;
    c.vkGetDeviceQueue(vk_data.dev, indices[0], 0, &vk_data.graphics_queue);
    c.vkGetDeviceQueue(vk_data.dev, indices[1], 0, &vk_data.present_queue);
}

fn createRenderPass() !void {
    const color_attach = c.VkAttachmentDescription {
        .format = vk_data.swapchain_fmt,
        .samples = vk_data.msaa,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };
    const depth_attach = c.VkAttachmentDescription {
        .format = try findSupportedFmt(&.{
            c.VK_FORMAT_D32_SFLOAT, c.VK_FORMAT_D32_SFLOAT_S8_UINT, c.VK_FORMAT_D24_UNORM_S8_UINT
        }, c.VK_IMAGE_TILING_OPTIMAL, c.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT),
        .samples = vk_data.msaa,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };
    const color_resolve = c.VkAttachmentDescription {
        .format = vk_data.swapchain_fmt,
        .samples = c.VK_SAMPLE_COUNT_1_BIT,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    const color_attach_ref = c.VkAttachmentReference {
        .attachment = 0,
        .layout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };
    const depth_attach_ref = c.VkAttachmentReference {
        .attachment = 1,
        .layout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };
    const color_resolve_ref = c.VkAttachmentReference {
        .attachment = 2,
        .layout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    const subpass = c.VkSubpassDescription {
        .pipelineBindPoint = c.VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attach_ref,
        .pDepthStencilAttachment = &depth_attach_ref,
        .pResolveAttachments = &color_resolve_ref,
    };

    const depend = c.VkSubpassDependency {
        .srcSubpass = c.VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        .srcAccessMask = 0,
        .dstStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        .dstAccessMask = c.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
    };

    const attachments = [_]c.VkAttachmentDescription{ color_attach, depth_attach, color_resolve };
    const render_pass_info = c.VkRenderPassCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = attachments.len,
        .pAttachments = &attachments,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &depend,
    };

    if (c.VK_SUCCESS != c.vkCreateRenderPass(vk_data.dev, &render_pass_info, null, &vk_data.render_pass))
        return VkError.RenderPassErr;
}

fn createDescriptorSetLayout() !void {
    const ubo_binding = c.VkDescriptorSetLayoutBinding {
        .binding = 0,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = c.VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = null,
    };

    const sampler_binding = c.VkDescriptorSetLayoutBinding {
        .binding = 1,
        .descriptorCount = 1,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImmutableSamplers = null,
        .stageFlags = c.VK_SHADER_STAGE_FRAGMENT_BIT,
    };

    const bindings = [_]c.VkDescriptorSetLayoutBinding{ ubo_binding, sampler_binding };

    const layout_info = c.VkDescriptorSetLayoutCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = bindings.len,
        .pBindings = &bindings,
    };

    if (c.VK_SUCCESS != c.vkCreateDescriptorSetLayout(vk_data.dev, &layout_info, null, &vk_data.descriptorset_layout))
        return VkError.UniformErr;
}

fn createGraphicsPipeline() !void {
    const vert_code align(4) = @embedFile("shaders/vert.spv").*;
    const vert_module = try createShaderModule(&vert_code);
    defer c.vkDestroyShaderModule(vk_data.dev, vert_module, null);

    const frag_code align(4) = @embedFile("shaders/frag.spv").*;
    const frag_module = try createShaderModule(&frag_code);
    defer c.vkDestroyShaderModule(vk_data.dev, frag_module, null);

    const vert_stage_info = c.VkPipelineShaderStageCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_VERTEX_BIT,
        .module = vert_module,
        .pName = "main",
    };
    const frag_stage_info = c.VkPipelineShaderStageCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = frag_module,
        .pName = "main",
    };
    const shader_stages = [2]c.VkPipelineShaderStageCreateInfo {vert_stage_info, frag_stage_info};

    const binding_description = Vertex.binding_description;
    const attrib_desciprtions = Vertex.attrib_descriptions;
    const vert_info = c.VkPipelineVertexInputStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .vertexAttributeDescriptionCount = attrib_desciprtions.len,
        .pVertexBindingDescriptions = &binding_description,
        .pVertexAttributeDescriptions = &attrib_desciprtions,
    };

    const input_assembly = c.VkPipelineInputAssemblyStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = c.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = c.VK_FALSE,
    };

    const viewport_state = c.VkPipelineViewportStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    const rasterizer = c.VkPipelineRasterizationStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = c.VK_FALSE,
        .rasterizerDiscardEnable = c.VK_FALSE,
        .polygonMode = c.VK_POLYGON_MODE_FILL,
        .lineWidth = 1,
        .cullMode = c.VK_CULL_MODE_BACK_BIT,
        .frontFace = c.VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = c.VK_FALSE,
        .depthBiasConstantFactor = 0,
        .depthBiasClamp = 0,
        .depthBiasSlopeFactor = 0,
    };

    const multisampling = c.VkPipelineMultisampleStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = c.VK_FALSE,
        .rasterizationSamples = vk_data.msaa,
    };

    const depth_stencil = c.VkPipelineDepthStencilStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = c.VK_TRUE,
        .depthWriteEnable = c.VK_TRUE,
        .depthCompareOp = c.VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = c.VK_FALSE,
        .stencilTestEnable = c.VK_FALSE,
    };

    const color_blend_attach = c.VkPipelineColorBlendAttachmentState {
        .colorWriteMask = c.VK_COLOR_COMPONENT_R_BIT | c.VK_COLOR_COMPONENT_G_BIT |
            c.VK_COLOR_COMPONENT_B_BIT | c.VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = c.VK_FALSE,
    };
    const color_blend = c.VkPipelineColorBlendStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = c.VK_FALSE,
        .logicOp = c.VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attach,
        .blendConstants = .{0, 0, 0, 0},
    };

    const dynamic_states = [_]c.VkDynamicState {
        c.VK_DYNAMIC_STATE_VIEWPORT,
        c.VK_DYNAMIC_STATE_SCISSOR,
    };
    const dynamic_state = c.VkPipelineDynamicStateCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = dynamic_states.len,
        .pDynamicStates = &dynamic_states,
    };

    const pipeline_layout = c.VkPipelineLayoutCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &vk_data.descriptorset_layout,
        // .pushConstantRangeCount = 0,
    };

    if (c.VK_SUCCESS != c.vkCreatePipelineLayout(vk_data.dev, &pipeline_layout, null, &vk_data.pipeline_layout))
        return VkError.PipelineErr;
    const pipeline_info = c.VkGraphicsPipelineCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = &shader_stages,
        .pVertexInputState = &vert_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_state,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisampling,
        .pDepthStencilState = &depth_stencil,
        .pColorBlendState = &color_blend,
        .pDynamicState = &dynamic_state,
        .layout = vk_data.pipeline_layout,
        .renderPass = vk_data.render_pass,
        .subpass = 0,
        .basePipelineHandle = null,
    };
    if (c.VK_SUCCESS != c.vkCreateGraphicsPipelines(vk_data.dev, null, 1, &pipeline_info, null, &vk_data.pipeline))
        return VkError.PipelineErr;
}
fn createShaderModule(code: []align(@alignOf(u32)) const u8) !c.VkShaderModule {
    const create_info = c.VkShaderModuleCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code.len,
        .pCode = std.mem.bytesAsSlice(u32, code).ptr,
    };

    var shader_module: c.VkShaderModule = undefined;
    return if (c.vkCreateShaderModule(vk_data.dev, &create_info, null, &shader_module) != c.VK_SUCCESS)
        VkError.ShaderErr else shader_module;
}

fn createFramebuffers() !void {
    vk_data.swapchain_framebuffs = try alloc.alloc(c.VkFramebuffer, vk_data.swapchain_views.len);

    for (vk_data.swapchain_views, 0..) |swapchain_view, i| {
        const attachments = [_]c.VkImageView{vk_data.color_view, vk_data.depth_view, swapchain_view};
        const framebuffer_info = c.VkFramebufferCreateInfo {
            .sType = c.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = vk_data.render_pass,
            .attachmentCount = attachments.len,
            .pAttachments = &attachments,
            .width = vk_data.swapchain_extent.width,
            .height = vk_data.swapchain_extent.height,
            .layers = 1,
        };

        if (c.vkCreateFramebuffer(vk_data.dev, &framebuffer_info, null, &vk_data.swapchain_framebuffs[i]) != c.VK_SUCCESS)
            return VkError.FrameBufferErr;
    }
}

fn createCmdPool(indices: [2]u32) !void {
    const pool_info = c.VkCommandPoolCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = c.VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = indices[0],
    };

    if (c.VK_SUCCESS != c.vkCreateCommandPool(vk_data.dev, &pool_info, null, &vk_data.cmd_pool))
        return VkError.CommandErr;
}

fn findSupportedFmt(candidates: []const c.VkFormat, tiling: c.VkImageTiling, features: c.VkFormatFeatureFlags) !c.VkFormat {
    for (candidates) |format| {
        var props: c.VkFormatProperties = undefined;
        c.vkGetPhysicalDeviceFormatProperties(vk_data.phys_dev, format, &props);

        if ((tiling == c.VK_IMAGE_TILING_LINEAR and (props.linearTilingFeatures & features) == features) or
            (tiling == c.VK_IMAGE_TILING_OPTIMAL and (props.optimalTilingFeatures & features) == features))
            return format;
    }
    return VkError.NoFormat; 
}
fn createDepthResources() !void {
    const depth_fmt: c.VkFormat = try findSupportedFmt(&.{
            c.VK_FORMAT_D32_SFLOAT, c.VK_FORMAT_D32_SFLOAT_S8_UINT, c.VK_FORMAT_D24_UNORM_S8_UINT
        }, c.VK_IMAGE_TILING_OPTIMAL, c.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);

    try createImg(@bitCast(vk_data.swapchain_extent), 1, vk_data.msaa,
        depth_fmt, c.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, &vk_data.depth_img, &vk_data.depth_mem);
    vk_data.depth_view = try createImgView(vk_data.depth_img, depth_fmt, c.VK_IMAGE_ASPECT_DEPTH_BIT, 1);
}

fn createColorResources() !void {
    try createImg(@bitCast(vk_data.swapchain_extent), 1, vk_data.msaa, vk_data.swapchain_fmt, c.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, &vk_data.color_img, &vk_data.color_mem);
    vk_data.color_view = try createImgView(vk_data.color_img, vk_data.swapchain_fmt, c.VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

fn createImg(exten: [2]u32, mip: u32, n_samples: c.VkSampleCountFlagBits, format: c.VkFormat, use: c.VkImageUsageFlags, img: *c.VkImage, mem: *c.VkDeviceMemory) !void {
    const image_info = c.VkImageCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = c.VK_IMAGE_TYPE_2D,
        .extent = .{
            .width = exten[0],
            .height = exten[1],
            .depth = 1,
        },
        .mipLevels = mip,
        .arrayLayers = 1,
        .format = format,
        .tiling = c.VK_IMAGE_TILING_OPTIMAL,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .usage = use,
        .sharingMode = c.VK_SHARING_MODE_EXCLUSIVE,
        .samples = n_samples,
    };

    if (c.vkCreateImage(vk_data.dev, &image_info, null, img) != c.VK_SUCCESS)
        return VkError.ImgErr;

    var mem_req: c.VkMemoryRequirements = undefined;
    c.vkGetImageMemoryRequirements(vk_data.dev, img.*, &mem_req);

    const alloc_info = c.VkMemoryAllocateInfo {
        .sType = c.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_req.size,
        .memoryTypeIndex = try findMemoryType(mem_req.memoryTypeBits, c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
    };

    if (c.vkAllocateMemory(vk_data.dev, &alloc_info, null, mem) != c.VK_SUCCESS)
        return VkError.ImgErr;

    _ = c.vkBindImageMemory(vk_data.dev, img.*, mem.*, 0);
}

fn createTexImg() !void {
    var tex_width: i32 = undefined;
    var tex_height: i32 = undefined;
    var channels: i32 = undefined;

    const pixels = c.stbi_load("assets/viking_room.png", &tex_width, &tex_height, &channels, c.STBI_rgb_alpha)
        orelse return VkError.TexErr;
    defer c.stbi_image_free(pixels);

    const exten = [2]u32{ @intCast(tex_width), @intCast(tex_height) };
    const img_size: u64 = 4 * @as(u64, exten[0] * exten[1]);
    vk_data.mip_levels = std.math.log2_int(u32, @max(exten[0], exten[1])) + 1;

    var staging_buff: c.VkBuffer = undefined;
    var staging_mem: c.VkDeviceMemory = undefined;
    try createBuffer(c.VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &staging_buff, &staging_mem, img_size);

    var data: ?*anyopaque = undefined;
    _ = c.vkMapMemory(vk_data.dev, staging_mem, 0, img_size, 0, &data);
    _ = c.memcpy(data, pixels, img_size);
    c.vkUnmapMemory(vk_data.dev, staging_mem);

    try createImg(exten, vk_data.mip_levels, c.VK_SAMPLE_COUNT_1_BIT, c.VK_FORMAT_R8G8B8A8_SRGB,
        c.VK_IMAGE_USAGE_TRANSFER_SRC_BIT | c.VK_IMAGE_USAGE_TRANSFER_DST_BIT | c.VK_IMAGE_USAGE_SAMPLED_BIT,
        &vk_data.tex_img, &vk_data.tex_mem);

    try transitionImgLayout(vk_data.tex_img, c.VK_IMAGE_LAYOUT_UNDEFINED,
        c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, vk_data.mip_levels);
    try copyBuffToImg(staging_buff, vk_data.tex_img, exten[0], exten[1]);
    // transitioned to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL while generating mipmaps
    // try transitionImgLayout(vk_data.tex_img, c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    //     c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    c.vkDestroyBuffer(vk_data.dev, staging_buff, null);
    c.vkFreeMemory(vk_data.dev, staging_mem, null);

    try genMipmaps(vk_data.tex_img, c.VK_FORMAT_R8G8B8A8_SRGB, exten, vk_data.mip_levels);
}

fn genMipmaps(image: c.VkImage, format: c.VkFormat, exten: [2]u32, mip: u32) !void {
    var format_props: c.VkFormatProperties = undefined;
    c.vkGetPhysicalDeviceFormatProperties(vk_data.phys_dev, format, &format_props);
    if ((format_props.optimalTilingFeatures & c.VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT) == 0)
        return VkError.TexErr;

    const cmd_buff = beginSingleTimeCmds();

    var barrier = c.VkImageMemoryBarrier {
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .image = image,
        .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .subresourceRange = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .baseArrayLayer = 0,
            .layerCount = 1,
            .levelCount = 1,
        },
    };

    var mip_w: i32 = @intCast(exten[0]);
    var mip_h: i32 = @intCast(exten[1]);

    var i: u32 = 1;
    while (i < mip) : (i += 1) {
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = c.VK_ACCESS_TRANSFER_READ_BIT;

        c.vkCmdPipelineBarrier(cmd_buff,
            c.VK_PIPELINE_STAGE_TRANSFER_BIT, c.VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
            0, null,
            0, null,
            1, &barrier);

        const blit = c.VkImageBlit {
            .srcOffsets = .{.{ .x = 0, .y = 0, .z = 0 }, .{ .x = mip_w, .y = mip_h, .z = 1 }},
            .srcSubresource = .{
                .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i - 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
            .dstOffsets = .{.{ .x = 0, .y = 0, .z = 0 }, .{ .x = @max(@divTrunc(mip_w, 2), 1), .y = @max(@divTrunc(mip_h, 2), 1), .z = 1 }},
            .dstSubresource = .{
                .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i,
                .baseArrayLayer = 0,
                .layerCount = 1,
            }
        };

        c.vkCmdBlitImage(cmd_buff, image, c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, c.VK_FILTER_LINEAR);

        barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.newLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_READ_BIT;
        barrier.dstAccessMask = c.VK_ACCESS_SHADER_READ_BIT;

        c.vkCmdPipelineBarrier(cmd_buff, c.VK_PIPELINE_STAGE_TRANSFER_BIT, c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, null, 0, null, 1, &barrier);

        if (mip_w > 1) mip_w = @divTrunc(mip_w, 2);
        if (mip_h > 1) mip_h = @divTrunc(mip_h, 2);
    }

    barrier.subresourceRange.baseMipLevel = mip - 1;
    barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = c.VK_ACCESS_SHADER_READ_BIT;

    c.vkCmdPipelineBarrier(cmd_buff, c.VK_PIPELINE_STAGE_TRANSFER_BIT, c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
        0, null, 0, null, 1, &barrier);

    endSingleTimeCmds(cmd_buff);
}

fn getMaxSampleCount() c.VkSampleCountFlagBits {
    var properties: c.VkPhysicalDeviceProperties = undefined;
    c.vkGetPhysicalDeviceProperties(vk_data.phys_dev, &properties);

    const counts = properties.limits.framebufferColorSampleCounts &
        properties.limits.framebufferDepthSampleCounts;
    if (counts & c.VK_SAMPLE_COUNT_64_BIT != 0) return c.VK_SAMPLE_COUNT_64_BIT;
    if (counts & c.VK_SAMPLE_COUNT_32_BIT != 0) return c.VK_SAMPLE_COUNT_32_BIT;
    if (counts & c.VK_SAMPLE_COUNT_16_BIT != 0) return c.VK_SAMPLE_COUNT_16_BIT;
    if (counts & c.VK_SAMPLE_COUNT_8_BIT != 0) return c.VK_SAMPLE_COUNT_8_BIT;
    if (counts & c.VK_SAMPLE_COUNT_4_BIT != 0) return c.VK_SAMPLE_COUNT_4_BIT;
    if (counts & c.VK_SAMPLE_COUNT_2_BIT != 0) return c.VK_SAMPLE_COUNT_2_BIT;
    return c.VK_SAMPLE_COUNT_1_BIT;
}

fn createTexSampler() !void {
    var props: c.VkPhysicalDeviceProperties = undefined;
    c.vkGetPhysicalDeviceProperties(vk_data.phys_dev, &props);

    const sampler_info = c.VkSamplerCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = c.VK_FILTER_LINEAR,
        .minFilter = c.VK_FILTER_LINEAR,
        .addressModeU = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .anisotropyEnable = c.VK_TRUE, // consider not enforcing
        .maxAnisotropy = props.limits.maxSamplerAnisotropy,
        .borderColor = c.VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = c.VK_FALSE,
        .compareEnable = c.VK_FALSE,
        .compareOp = c.VK_COMPARE_OP_ALWAYS,
        .mipmapMode = c.VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .minLod = 0,
        .maxLod = @floatFromInt(vk_data.mip_levels),
        .mipLodBias = 0,
    };

    if (c.VK_SUCCESS != c.vkCreateSampler(vk_data.dev, &sampler_info, null, &vk_data.tex_sampler))
        return VkError.TexErr;
}

fn loadModel() !void {
    const mesh = c.fast_obj_read("assets/viking_room.obj") orelse return VkError.ModelErr;
    defer c.fast_obj_destroy(mesh);

    std.debug.assert(mesh.*.index_count == 3 * mesh.*.face_count);
    std.debug.assert(mesh.*.position_count - 1 <= mesh.*.index_count);

    var map = std.AutoHashMap([2]u32, u32).init(alloc); // ArrayHashMap and/or Unmanaged may be better
    try map.ensureTotalCapacity(@max(mesh.*.position_count, mesh.*.texcoord_count) - 1);
    defer map.deinit();

    vk_data.indices = try alloc.alloc(u32, mesh.*.index_count);
    for (mesh.*.indices, vk_data.indices) |data, *idx| {
        std.debug.assert(data.p != 0 and data.t != 0);
        const vert_indices = [2]u32{ data.p, data.t};
        if (map.get(vert_indices)) |i| {
            idx.* = i;
        } else {
            idx.* = @truncate(map.count());
            try map.put(vert_indices, idx.*);
        }
    }

    vk_data.vertices = try alloc.alloc(Vertex, map.count());
    var iter = map.iterator();
    while (iter.next()) |e| {
        vk_data.vertices[e.value_ptr.*] = .{
            .pos = .{mesh.*.positions[3*e.key_ptr.*[0]], mesh.*.positions[3*e.key_ptr.*[0] + 1], mesh.*.positions[3*e.key_ptr.*[0] + 2]},
            .color = .{1, 1, 1},
            .tex_coord = .{mesh.*.texcoords[2*e.key_ptr.*[1]], 1 - mesh.*.texcoords[2*e.key_ptr.*[1] + 1]},
        };
    }
}

fn createVertBuff() !void {
    var staging_buff: c.VkBuffer = undefined;
    var staging_mem: c.VkDeviceMemory = undefined;
    try createBuffer(c.VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &staging_buff, &staging_mem, vertSize());

    var data: ?*anyopaque = undefined;
    _ = c.vkMapMemory(vk_data.dev, staging_mem, 0, vertSize(), 0, &data);
    // TODO: figure out how to do with zig, not C
    _ = c.memcpy(data, &vk_data.vertices[0], vertSize());
    c.vkUnmapMemory(vk_data.dev, staging_mem);

    try createBuffer(c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &vk_data.vert_buff, &vk_data.vert_mem, vertSize());

    copyBuffer(vk_data.vert_buff, staging_buff, vertSize());

    c.vkDestroyBuffer(vk_data.dev, staging_buff, null);
    c.vkFreeMemory(vk_data.dev, staging_mem, null);
}
// TODO: find more performant memory
fn findMemoryType(filter: u32, props: c.VkMemoryPropertyFlags) !u32 {
    var mem_props: c.VkPhysicalDeviceMemoryProperties = undefined;
    c.vkGetPhysicalDeviceMemoryProperties(vk_data.phys_dev, &mem_props);
    for (mem_props.memoryTypes, 0..) |prop, i| {
        if (filter & (@as(u32, 1) << @truncate(i)) != 0 and prop.propertyFlags & props == props)
            return @truncate(i);
    }
    return VkError.BuffErr;
}
fn createBuffer(usage: c.VkBufferUsageFlags, props: c.VkMemoryPropertyFlags,
    buffer: *c.VkBuffer, buff_mem: *c.VkDeviceMemory, size: u64) !void {
    const buff_info = c.VkBufferCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = c.VK_SHARING_MODE_EXCLUSIVE,
    };

    if (c.vkCreateBuffer(vk_data.dev, &buff_info, null, buffer) != c.VK_SUCCESS)
        return VkError.BuffErr;

    var mem_req: c.VkMemoryRequirements = undefined;
    c.vkGetBufferMemoryRequirements(vk_data.dev, buffer.*, &mem_req);

    const alloc_info = c.VkMemoryAllocateInfo {
        .sType = c.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_req.size,
        .memoryTypeIndex = try findMemoryType(mem_req.memoryTypeBits, props),
    };

    if (c.vkAllocateMemory(vk_data.dev, &alloc_info, null, buff_mem) != c.VK_SUCCESS)
        return VkError.BuffErr;

    if (c.vkBindBufferMemory(vk_data.dev, buffer.*, buff_mem.*, 0) != c.VK_SUCCESS)
        return VkError.BuffErr;
}
fn copyBuffer(dest: c.VkBuffer, src: c.VkBuffer, size: c.VkDeviceSize) void {
    const cmd_buff = beginSingleTimeCmds();

    const cpy_region = c.VkBufferCopy{ .size = size };
    c.vkCmdCopyBuffer(cmd_buff, src, dest, 1, &cpy_region);

    endSingleTimeCmds(cmd_buff);
}

fn transitionImgLayout(image: c.VkImage, old_layout: c.VkImageLayout, new_layout: c.VkImageLayout, mip: u32) !void {
    const cmd_buff = beginSingleTimeCmds();

    var barrier = c.VkImageMemoryBarrier {
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = old_layout,
        .newLayout = new_layout,
        .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = mip,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    var src_stage: c.VkPipelineStageFlags = undefined;
    var dst_stage: c.VkPipelineStageFlags = undefined;

    if (old_layout == c.VK_IMAGE_LAYOUT_UNDEFINED and new_layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
        src_stage = c.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dst_stage = c.VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (old_layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL and new_layout == c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = c.VK_ACCESS_SHADER_READ_BIT;
        src_stage = c.VK_PIPELINE_STAGE_TRANSFER_BIT;
        dst_stage = c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        return VkError.TexErr;
    }

    c.vkCmdPipelineBarrier(
        cmd_buff,
        src_stage, dst_stage,
        0,
        0, null,
        0, null,
        1, &barrier,
    );

    endSingleTimeCmds(cmd_buff);
}

fn copyBuffToImg(buff: c.VkBuffer, img: c.VkImage, width: u32, height: u32) !void {
    const cmd_buff = beginSingleTimeCmds();

    const region = c.VkBufferImageCopy {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .imageOffset = .{ .x = 0, .y = 0, .z = 0 },
        .imageExtent = .{ .width = width, .height = height, .depth = 1 },
    };

    c.vkCmdCopyBufferToImage(
        cmd_buff,
        buff,
        img,
        c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    endSingleTimeCmds(cmd_buff);
}

fn beginSingleTimeCmds() c.VkCommandBuffer {
    const alloc_info = c.VkCommandBufferAllocateInfo {
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandPool = vk_data.cmd_pool,
        .commandBufferCount = 1,
    };

    var cmd_buff: c.VkCommandBuffer = undefined;
    _ = c.vkAllocateCommandBuffers(vk_data.dev, &alloc_info, &cmd_buff);

    const begin_info = c.VkCommandBufferBeginInfo {
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = c.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    _ = c.vkBeginCommandBuffer(cmd_buff, &begin_info);
    return cmd_buff;
}
fn endSingleTimeCmds(cmd_buff: c.VkCommandBuffer) void {
    _ = c.vkEndCommandBuffer(cmd_buff);

    const submit_info = c.VkSubmitInfo {
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &cmd_buff,
    };

    _ = c.vkQueueSubmit(vk_data.graphics_queue, 1, &submit_info, null);
    _ = c.vkQueueWaitIdle(vk_data.graphics_queue);
    c.vkFreeCommandBuffers(vk_data.dev, vk_data.cmd_pool, 1, &cmd_buff);
}

fn createIdxBuff() !void {
    var staging_buff: c.VkBuffer = undefined;
    var staging_mem: c.VkDeviceMemory = undefined;
    try createBuffer(c.VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &staging_buff, &staging_mem, idxSize());

    var data: ?*anyopaque = undefined;
    _ = c.vkMapMemory(vk_data.dev, staging_mem, 0, idxSize(), 0, &data);
    // TODO: figure out how to do with zig, not C
    _ = c.memcpy(data, &vk_data.indices[0], idxSize());
    c.vkUnmapMemory(vk_data.dev, staging_mem);

    try createBuffer(c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &vk_data.idx_buff, &vk_data.idx_mem, idxSize());

    copyBuffer(vk_data.idx_buff, staging_buff, idxSize());

    c.vkDestroyBuffer(vk_data.dev, staging_buff, null);
    c.vkFreeMemory(vk_data.dev, staging_mem, null);
}

fn createUniformBuffs() !void {
    for (&vk_data.uniform_buffs, &vk_data.uniform_mem, &vk_data.uniform_mapped) |*buff, *mem, *mapped| {
        try createBuffer(c.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
            c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, buff, mem, UNI_SIZE);
        _ = c.vkMapMemory(vk_data.dev, mem.*, 0, UNI_SIZE, 0, mapped);
    }
}

fn createDescriptorPool() !void {
    const pool_sizes = [_]c.VkDescriptorPoolSize {
        .{
            .type = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = MAX_IN_FLIGHT,
        },
        .{
            .type = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = MAX_IN_FLIGHT,
        },
    };
    const pool_info = c.VkDescriptorPoolCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .poolSizeCount = pool_sizes.len,
        .pPoolSizes = &pool_sizes,
        .maxSets = MAX_IN_FLIGHT,
    };

    if (c.VK_SUCCESS != c.vkCreateDescriptorPool(vk_data.dev, &pool_info, null, &vk_data.descriptor_pool))
        return VkError.DescriptorErr;
}

fn createDescriptorSets() !void {
    const layouts: [MAX_IN_FLIGHT]c.VkDescriptorSetLayout = .{vk_data.descriptorset_layout} ** MAX_IN_FLIGHT;
    const alloc_info = c.VkDescriptorSetAllocateInfo {
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = vk_data.descriptor_pool,
        .descriptorSetCount = MAX_IN_FLIGHT,
        .pSetLayouts = &layouts,
    };

    if (c.VK_SUCCESS != c.vkAllocateDescriptorSets(vk_data.dev, &alloc_info, &vk_data.descrptor_sets))
        return VkError.DescriptorErr;

    for (vk_data.uniform_buffs, vk_data.descrptor_sets) |buff, set| {
        const buff_info = c.VkDescriptorBufferInfo {
            .buffer = buff,
            .offset = 0,
            .range = UNI_SIZE,
        };
        const img_info = c.VkDescriptorImageInfo {
            .imageLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            .imageView = vk_data.tex_view,
            .sampler = vk_data.tex_sampler,
        };
        const descriptor_writes = [_]c.VkWriteDescriptorSet {
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = set,
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .pBufferInfo = &buff_info,
            },
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = set,
                .dstBinding = 1,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .pImageInfo = &img_info,
            },
        };
        c.vkUpdateDescriptorSets(vk_data.dev, descriptor_writes.len, &descriptor_writes, 0, null);
    }
}

fn createCmdBuffs() !void {
    const alloc_info = c.VkCommandBufferAllocateInfo {
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = vk_data.cmd_pool,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = vk_data.cmd_buffs.len,
    };
    if (c.vkAllocateCommandBuffers(vk_data.dev, &alloc_info, &vk_data.cmd_buffs) != c.VK_SUCCESS)
        return VkError.CommandErr;
}

fn recordCmdBuff(cmd_buff: c.VkCommandBuffer, img_idx: u32) !void {
    const begin_info = c.VkCommandBufferBeginInfo {
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
    };
    if (c.vkBeginCommandBuffer(cmd_buff, &begin_info) != c.VK_SUCCESS)
        return VkError.CommandErr;

    const clear_vals = [2]c.VkClearValue{.{ .color = .{ .float32 = .{0, 0, 0, 1} } },
        .{ .depthStencil = .{ .depth = 1, .stencil = 0} }};
    const pass_info = c.VkRenderPassBeginInfo {
        .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = vk_data.render_pass,
        .framebuffer = vk_data.swapchain_framebuffs[img_idx],
        .renderArea = .{
            .offset = .{ .x = 0, .y = 0 },
            .extent = vk_data.swapchain_extent,
        },
        .clearValueCount = clear_vals.len,
        .pClearValues = &clear_vals,
    };

    c.vkCmdBeginRenderPass(cmd_buff, &pass_info, c.VK_SUBPASS_CONTENTS_INLINE);
    c.vkCmdBindPipeline(cmd_buff, c.VK_PIPELINE_BIND_POINT_GRAPHICS, vk_data.pipeline);

    const viewport = c.VkViewport {
        .x = 0,
        .y = 0,
        .width = @floatFromInt(vk_data.swapchain_extent.width),
        .height = @floatFromInt(vk_data.swapchain_extent.height),
        .minDepth = 0,
        .maxDepth = 1,
    };
    c.vkCmdSetViewport(cmd_buff, 0, 1, &viewport);

    const scissor = c.VkRect2D {
        .offset = .{ .x = 0, .y = 0 },
        .extent = vk_data.swapchain_extent,
    };
    c.vkCmdSetScissor(cmd_buff, 0, 1, &scissor);

    const offset: u64 = 0;
    c.vkCmdBindVertexBuffers(cmd_buff, 0, 1, &vk_data.vert_buff, &offset);

    c.vkCmdBindIndexBuffer(cmd_buff, vk_data.idx_buff, 0, c.VK_INDEX_TYPE_UINT32);
    c.vkCmdBindDescriptorSets(cmd_buff, c.VK_PIPELINE_BIND_POINT_GRAPHICS,
        vk_data.pipeline_layout, 0, 1, &vk_data.descrptor_sets[vk_data.current_frame], 0, null);

    c.vkCmdDrawIndexed(cmd_buff, @truncate(vk_data.indices.len), 1, 0, 0, 0);
    c.vkCmdEndRenderPass(cmd_buff);
    if (c.vkEndCommandBuffer(cmd_buff) != c.VK_SUCCESS)
        return VkError.CommandErr;
}

fn createSyncObjs() !void {
    const semaphore_info = c.VkSemaphoreCreateInfo{ .sType = c.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };
    const fence_info = c.VkFenceCreateInfo {
        .sType = c.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = c.VK_FENCE_CREATE_SIGNALED_BIT,
    };

    for (&vk_data.img_semaphores, &vk_data.render_semaphores, &vk_data.in_flight_fences) |*img, *rend, *fence| {
        if (c.vkCreateSemaphore(vk_data.dev, &semaphore_info, null, img) != c.VK_SUCCESS or
            c.vkCreateSemaphore(vk_data.dev, &semaphore_info, null, rend) != c.VK_SUCCESS or
            c.vkCreateFence(vk_data.dev, &fence_info, null, fence) != c.VK_SUCCESS)
            return VkError.SyncErr;
    }
    vk_data.current_frame = 0;
}
///// End Setup /////

pub fn mainLoop() !void {
    vk_data.timer = try std.time.Timer.start();
    while (c.glfwWindowShouldClose(vk_data.window) == 0) {
        c.glfwPollEvents();
        try drawFrame();
    }
    _ = c.vkDeviceWaitIdle(vk_data.dev);
}

fn drawFrame() !void {
    _ = c.vkWaitForFences(vk_data.dev, 1, &vk_data.in_flight_fences[vk_data.current_frame],
        c.VK_TRUE, std.math.maxInt(u64));
    
    var img_idx: u32 = undefined;
    _ = c.vkAcquireNextImageKHR(vk_data.dev, vk_data.swapchain, std.math.maxInt(u64),
        vk_data.img_semaphores[vk_data.current_frame], null, &img_idx);

    updateUniformBuff();

    _ = c.vkResetFences(vk_data.dev, 1, &vk_data.in_flight_fences[vk_data.current_frame]);

    _ = c.vkResetCommandBuffer(vk_data.cmd_buffs[vk_data.current_frame], 0);
    try recordCmdBuff(vk_data.cmd_buffs[vk_data.current_frame], img_idx);

    const wait_stage: u32 = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    const submit_info = c.VkSubmitInfo {
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &vk_data.img_semaphores[vk_data.current_frame],
        .pWaitDstStageMask = &wait_stage,
        .commandBufferCount = 1,
        .pCommandBuffers = &vk_data.cmd_buffs[vk_data.current_frame],
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &vk_data.render_semaphores[vk_data.current_frame],
    };

    if (c.vkQueueSubmit(vk_data.graphics_queue, 1, &submit_info, vk_data.in_flight_fences[vk_data.current_frame]) != c.VK_SUCCESS)
        return VkError.CommandErr;

    const present_info = c.VkPresentInfoKHR {
        .sType = c.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &vk_data.render_semaphores[vk_data.current_frame],
        .swapchainCount = 1,
        .pSwapchains = &vk_data.swapchain,
        .pImageIndices = &img_idx,
    };
    _ = c.vkQueuePresentKHR(vk_data.present_queue, &present_info);

    vk_data.current_frame = (vk_data.current_frame + 1) % MAX_IN_FLIGHT;
}

fn updateUniformBuff() void {
    const delta_t = 1e-9 * @as(f32, @floatFromInt(vk_data.timer.read()));
    const mvp = math.mul(
        math.mul(math.rotateZ(@sin(0.5*delta_t) * std.math.pi / 4), math.lookAt(.{2, 2, 2}, .{0, 0, 0}, .{0, 0, 1})),
        math.perspec(0.25 * std.math.pi, math.div(f32, vk_data.swapchain_extent.width, vk_data.swapchain_extent.height), 0.1, 10)
    );
    _ = c.memcpy(vk_data.uniform_mapped[vk_data.current_frame], &mvp, UNI_SIZE);
}

pub fn deinit() void {
    c.vkDestroyImageView(vk_data.dev, vk_data.color_view, null);
    c.vkDestroyImage(vk_data.dev, vk_data.color_img, null);
    c.vkFreeMemory(vk_data.dev, vk_data.color_mem, null);

    c.vkDestroyImageView(vk_data.dev, vk_data.depth_view, null);
    c.vkDestroyImage(vk_data.dev, vk_data.depth_img, null);
    c.vkFreeMemory(vk_data.dev, vk_data.depth_mem, null);

    for (vk_data.swapchain_framebuffs) |framebuffer|
        c.vkDestroyFramebuffer(vk_data.dev, framebuffer, null);

    c.vkDestroySampler(vk_data.dev, vk_data.tex_sampler, null);

    for (vk_data.swapchain_views) |view|
        c.vkDestroyImageView(vk_data.dev, view, null);

    c.vkDestroyImageView(vk_data.dev, vk_data.tex_view, null);
    alloc.free(vk_data.swapchain_views);

    c.vkDestroySwapchainKHR(vk_data.dev, vk_data.swapchain, null);
    alloc.free(vk_data.swapchain_imgs);
    // Consider arena for these allocs

    c.vkDestroyPipeline(vk_data.dev, vk_data.pipeline, null);
    c.vkDestroyPipelineLayout(vk_data.dev, vk_data.pipeline_layout, null);
    c.vkDestroyRenderPass(vk_data.dev, vk_data.render_pass, null);

    for (vk_data.uniform_buffs, vk_data.uniform_mem) |buff, mem| {
        c.vkDestroyBuffer(vk_data.dev, buff, null);
        c.vkFreeMemory(vk_data.dev, mem, null);
    }

    c.vkDestroyImage(vk_data.dev, vk_data.tex_img, null);
    c.vkFreeMemory(vk_data.dev, vk_data.tex_mem, null);

    c.vkDestroyDescriptorPool(vk_data.dev, vk_data.descriptor_pool, null);
    c.vkDestroyDescriptorSetLayout(vk_data.dev, vk_data.descriptorset_layout, null);

    c.vkDestroyBuffer(vk_data.dev, vk_data.idx_buff, null);
    c.vkFreeMemory(vk_data.dev, vk_data.idx_mem, null);
    c.vkDestroyBuffer(vk_data.dev, vk_data.vert_buff, null);
    c.vkFreeMemory(vk_data.dev, vk_data.vert_mem, null);

    for (vk_data.img_semaphores, vk_data.render_semaphores) |img, rend| {
        c.vkDestroySemaphore(vk_data.dev, rend, null);
        c.vkDestroySemaphore(vk_data.dev, img, null);
    }
    for (vk_data.in_flight_fences) |fence|
        c.vkDestroyFence(vk_data.dev, fence, null);

    c.vkDestroyCommandPool(vk_data.dev, vk_data.cmd_pool, null);

    c.vkDestroyDevice(vk_data.dev, null);
    if (DEBUG_ENABLE) {
        if (debugFn(c.PFN_vkDestroyDebugUtilsMessengerEXT, "vkDestroyDebugUtilsMessengerEXT")) |vk_fn|
            vk_fn(vk_data.instance, debug_messenger, null);
    }

    alloc.free(vk_data.vertices);
    alloc.free(vk_data.indices);

    c.vkDestroySurfaceKHR(vk_data.instance, vk_data.surface, null);
    c.vkDestroyInstance(vk_data.instance, null);

    c.glfwDestroyWindow(vk_data.window);
    c.glfwTerminate();
}

// Debug
fn debugFn(comptime T: type, comptime fn_name: [*c]const u8) T {
    return @ptrCast(c.glfwGetInstanceProcAddress(vk_data.instance, fn_name));
}

const validation_layers = [_][]const u8{"VK_LAYER_KHRONOS_validation"};
var debug_messenger: c.VkDebugUtilsMessengerEXT = undefined;
fn checkValidationLayers() !bool {
    var layer_count: u32 = undefined;
    _ = c.vkEnumerateInstanceLayerProperties(&layer_count, null);
    const avail_layers = try alloc.alloc(c.VkLayerProperties, layer_count);
    defer alloc.free(avail_layers);
    _ = c.vkEnumerateInstanceLayerProperties(&layer_count, @ptrCast(avail_layers));
    for (validation_layers) |layer| {
        for (avail_layers) |avail| {
            if (cstreql(&avail.layerName, layer)) break;
        } else return false;
    }
    return true;
}

fn debugCallback(
    message_severety: c.VkDebugUtilsMessageSeverityFlagBitsEXT,
    message_type: c.VkDebugUtilsMessageTypeFlagsEXT,
    callback_data: [*c]const c.VkDebugUtilsMessengerCallbackDataEXT,
    user_data: ?*anyopaque
) callconv(.C) c.VkBool32 {
    _ = user_data;
    _ = message_type;
    if (message_severety >= c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        std.debug.print("validation layer: {s}", .{callback_data.*.pMessage});
    return c.VK_FALSE;
}
const debug_messenger_create_info = if (DEBUG_ENABLE) c.VkDebugUtilsMessengerCreateInfoEXT {
    .sType = c.VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
    .messageSeverity = c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
    .messageType = c.VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        c.VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
        c.VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
    .pfnUserCallback = debugCallback,
    // .pUserData = null,
};
fn setupDebugMessenger() !void {
    if ((debugFn(c.PFN_vkCreateDebugUtilsMessengerEXT, "vkCreateDebugUtilsMessengerEXT")
                orelse return VkError.DebugMessenger)
            (vk_data.instance, &debug_messenger_create_info, null, &debug_messenger) != c.VK_SUCCESS)
        return VkError.DebugMessenger;
}

const Vertex = struct {
    pos: [3]f32,
    color: [3]f32,
    tex_coord: [2]f32,
    
    const binding_description = c.VkVertexInputBindingDescription {
        .binding = 0,
        .stride = @sizeOf(@This()),
        .inputRate = c.VK_VERTEX_INPUT_RATE_VERTEX,
    };
    const attrib_descriptions = [3]c.VkVertexInputAttributeDescription {
        .{
            .binding = 0,
            .location = 0,
            .format = c.VK_FORMAT_R32G32B32_SFLOAT,
            .offset = @offsetOf(@This(), "pos"),
        },
        .{
            .binding = 0,
            .location = 1,
            .format = c.VK_FORMAT_R32G32B32_SFLOAT,
            .offset = @offsetOf(@This(), "color"),
        },
        .{
            .binding = 0,
            .location = 2,
            .format = c.VK_FORMAT_R32G32_SFLOAT,
            .offset = @offsetOf(@This(), "tex_coord"),
        },
    };
};

test "vk start/stop" {
    const test_alloc = std.testing.allocator;
    try init(test_alloc);
    deinit(test_alloc);
}
